class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(player={})
    @guesser = player[:guesser]
    @referee = player[:referee]
  end

  def setup
      length = referee.pick_secret_word
      # guesser
      guesser.register_secret_length(length)
      @board = Array.new(length)
  end

  def take_turn
    guess = guesser.guess
    referee.check_guess(guess)
    update_board(guess)
    guesser.handle_response(board, guess)
  end

  def update_board(guess)
    referee.check_guess(guess).each {|i| board[i] = guess}
  end

end

class HumanPlayer

  def register_secret_length(length)
    length
  end

  def pick_secret_word
    word.length
  end

  def word
    gets.chomp
  end

  def guess
  end

end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def word
    @dictionary.sample
  end

  def pick_secret_word
    word.length
  end

  def guess(board)
    # alpha = ('a'..'z').to_a
    # alpha.sample
    most_common_letters = Hash.new(0)
    if board.uniq == nil
      candidate_words.each do |word|
        word.chars.each {|let| most_common_letters[let] += 1}
      end
      return most_common_letters.sort_by {|k, v| v}.last.first
    else
      candidate_words.each do |word|
        word.chars.each {|let| most_common_letters[let] += 1}
      end
      return most_common_letters.sort_by {|k, v| v}.last.first
    end
  end

  def handle_response(guess, indices)
    # find the index of every instance of the letter stored as guess within the candidate variable word
    # compar the list of indices to i
    #if they are different, the word cannot be kept
    candidate_words.select! do |word|
      word_indices = []
      word.chars.each.with_index {|let, i| word_indices << i if let == guess}
      word_indices == indices
    end

  end

  def check_guess(guess)
      word.chars.each_index.select {|i| word[i] == guess }
  end

  def candidate_words
    @dictionary
  end

  def register_secret_length(length)
    candidate_words.reject! do |word|
      word.length != length
    end
    candidate_words
  end
end
